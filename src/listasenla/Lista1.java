/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenla;

/**
 *
 * @author Rodrigo Balan
 */
public class Lista1 {

    protected Nodo1 inicio, fin;   // Punteros para donde esta el inicio y el fin

    public Lista1() {
        inicio = null;
        fin = null;
    }

    public void agregarInicio(String elemento) {
        inicio = new Nodo1(elemento, inicio);
        if (fin == null) {
            fin = inicio;
        }
    }

    public void mostrarEnlazada() {
        Nodo1 recorre = inicio;
        System.out.println("");
        while (recorre != null) {
            System.out.print("[" + recorre.nombre + "] -->");
            recorre = recorre.siguiente;
        }
        System.out.println("");
    }

    // Metodo para eliminar el nodo del inicio
    public String borrarInicio() {
        String elemento = inicio.nombre;
        if (inicio == fin) {
            inicio = null;
            fin = null;
        } else {
            inicio = inicio.siguiente;
        }
        return elemento;
    }
}
