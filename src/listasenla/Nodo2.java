/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenla;

/**
 *
 * @author Rodrigo Balan
 */
public class Nodo2 {

    public String nombre;    // Valor que se va almacenar
    public Nodo2 siguiente; // Puntero (Mismo tipo de la clase)

    // Constructor para insertar los datos
    public Nodo2(String d) {
        this.nombre = d;
    }

    // Constructor para insertar el inicio de la lista
    public Nodo2(String d, Nodo2 n) {
        nombre = d;
        siguiente = n;
    }
}
