/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenla;

/**
 *
 * @author Rodrigo Balan
 */
public class Lista {

    protected Nodo inicio, fin;   // Punteros para donde esta el inicio y el fin

    public Lista() {
        inicio = null;
        fin = null;
    }

    public void agregarInicio(int elemento) {
        inicio = new Nodo(elemento, inicio);
        if (fin == null) {
            fin = inicio;
        }
    }

    public void mostrarEnlazada() {
        Nodo recorre = inicio;
        System.out.println("");
        while (recorre != null) {
            System.out.print("[" + recorre.dato + "] -->");
            recorre = recorre.siguiente;
        }
        System.out.println("");
    }

    // Metodo para eliminar el nodo del inicio
    public int borrarInicio() {
        int elemento = inicio.dato;
        if (inicio == fin) {
            inicio = null;
            fin = null;
        } else {
            inicio = inicio.siguiente;
        }
        return elemento;
    }
}
