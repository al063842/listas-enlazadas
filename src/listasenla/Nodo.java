/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenla;

/**
 *
 * @author Rodrigo Balan
 */
public class Nodo {

    public int dato;    // Valor que se va almacenar
    public Nodo siguiente;  // Puntero (Mismo tipo de la clase)

    // Constructor para insertar los datos
    public Nodo(int d) {
        this.dato = d;
    }

    // Constructor para insertar el inicio de la lista
    public Nodo(int d, Nodo n) {
        dato = d;
        siguiente = n;
    }
}
