/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listasenla;

/**
 *
 * @author Rodrigo Balan
 */
public class ListasEnla {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Lista2 listaEnlazada2 = new Lista2();
        Lista listaEnlazada = new Lista();
        Lista1 listaEnlazada1 = new Lista1();

        System.out.println("Gaytan Balan Rodrigo Manuel - 63842");

        System.out.println("Ejercicio 1 - Nombres de compañeros");

        listaEnlazada2.agregarInicio2("Cristian");
        listaEnlazada2.agregarInicio2("Frida");
        listaEnlazada2.agregarInicio2("Raul");
        listaEnlazada2.agregarInicio2("Darlin");
        listaEnlazada2.agregarInicio2("Gustavo");
        listaEnlazada2.agregarInicio2("Alexander");
        listaEnlazada2.agregarInicio2("Daniel");
        listaEnlazada2.agregarInicio2("Josue");
        listaEnlazada2.agregarInicio2("Orlando");
        listaEnlazada2.agregarInicio2("Jose");

        listaEnlazada2.mostrarEnlazada2();
        listaEnlazada2.borrarInicio2();
        listaEnlazada2.mostrarEnlazada2();
        listaEnlazada2.borrarInicio2();
        listaEnlazada2.borrarInicio2();
        listaEnlazada2.mostrarEnlazada2();

        listaEnlazada2.borrarInicio2();
        listaEnlazada2.borrarInicio2();
        listaEnlazada2.mostrarEnlazada2();

        System.out.println("Ejercicio 2 - Nombres de compañeros y edades");

        listaEnlazada1.agregarInicio("Cristian");
        listaEnlazada1.agregarInicio("Frida");
        listaEnlazada1.agregarInicio("Raul");
        listaEnlazada1.agregarInicio("Darlin");
        listaEnlazada1.agregarInicio("Gustavo");
        listaEnlazada1.agregarInicio("Alexander");
        listaEnlazada1.agregarInicio("Daniel");
        listaEnlazada1.agregarInicio("Josue");
        listaEnlazada1.agregarInicio("Orlando");
        listaEnlazada1.agregarInicio("Jose");

        listaEnlazada.agregarInicio(20);
        listaEnlazada.agregarInicio(20);
        listaEnlazada.agregarInicio(20);
        listaEnlazada.agregarInicio(20);
        listaEnlazada.agregarInicio(20);
        listaEnlazada.agregarInicio(20);
        listaEnlazada.agregarInicio(20);
        listaEnlazada.agregarInicio(20);
        listaEnlazada.agregarInicio(20);
        listaEnlazada.agregarInicio(20);

        listaEnlazada1.mostrarEnlazada();
        listaEnlazada1.borrarInicio();
        listaEnlazada1.mostrarEnlazada();
        listaEnlazada1.borrarInicio();
        listaEnlazada1.borrarInicio();
        listaEnlazada1.mostrarEnlazada();

        listaEnlazada1.borrarInicio();
        listaEnlazada1.borrarInicio();
        listaEnlazada1.mostrarEnlazada();

        listaEnlazada.mostrarEnlazada();
        listaEnlazada.borrarInicio();
        listaEnlazada.mostrarEnlazada();
        listaEnlazada.borrarInicio();
        listaEnlazada.borrarInicio();
        listaEnlazada.mostrarEnlazada();

        listaEnlazada.borrarInicio();
        listaEnlazada.borrarInicio();
        listaEnlazada.mostrarEnlazada();

    }

}
